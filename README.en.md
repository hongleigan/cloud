#  The Cloud Museum[[A/中]](README.md)

##  󾠮Introduction

 This is an HTML project to record the sky and clouds, which is designed to call on people to reduce the pollution of the atmosphere and feel the beauty of nature. In the writing process of this project, I tried to leave PC and use the development tools on Android system to complete the development. This project adopts GPL3.0 protocol, you can continue to expand and improve the project.

 ## 󾠯 Software Architecture

 This project is written in HTML/CSS, and all the code is written by myself.

 ## 󾠰Demo

 [demo online](https://hongleigan.gitee.io/cloud/)(build with gitee pages.)

## 󾠱The Screenshot Of The Project



![img/jt1.jpeg](img/jt1.jpeg)



![img/jt2.jpeg](img/jt2.jpeg)



![img/jt3.jpeg](img/jt3.jpeg)



## 󾠲Tips

This project cannot be adapted, and the PC side displays it in the horizontal screen state of the web page. This project is a little slow to load and needs to be optimized later. My skill is not very good,I am sorry.

```
<The Clouds Museum,a website of introducing the clouds>  Copyright (C) <2021>  <Honglei Gan
This program is free software: you can redistribute it and/or modifyit under the terms of the GNU General Public License as published bythe Free Software Foundation, either version 3 of the License, or(at your option) any later version.
This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>
```