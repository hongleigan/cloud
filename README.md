# 云朵博物馆[[中/A]](README.en.md)
##  󾠮简介
 这是一个用来记录天空和云朵的html项目，旨在号召人们减少对大气的污染，感受自然之美。此项目在编写过程中尝试脱离PC，完全使用安卓系统下的开发工具完成开发。此项目采用[GPL3.0协议](https://www.gnu.org/licenses/)，您可以在此项目上继续拓展完善。
 ## 󾠯软件架构
 此项目采用html/css编写，所有代码均为本人亲自编写。
 ## 󾠰Demo
 [在线demo](https://hongleigan.gitee.io/cloud/)(使用gitee pages.)
## 󾠱项目截图

![jt1.jpeg](img/jt1.jpeg)

![jt2.jpeg](img/jt2.jpeg)

![jt3.jpeg](img/jt3.jpeg)

## 󾠲说明
此项目无法自适应，PC端显示为网页横屏状态。此项目加载略慢，后期优化。
本人技艺不精，请诸位包涵。(::>_<::)
```
<The Clouds Museum,a website of introducing the clouds>  Copyright (C) <2021>  <Honglei Gan
This program is free software: you can redistribute it and/or modifyit under the terms of the GNU General Public License as published bythe Free Software Foundation, either version 3 of the License, or(at your option) any later version.
This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>
```